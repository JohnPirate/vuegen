<?php

/*
|---------------------------------------------------------------------------
| Requires composer autoload
|---------------------------------------------------------------------------
*/

use Symfony\Component\Console\Application;

$application = new Application();

$config = [
    'current_working_directory' => getcwd(),
    'templates_path' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'templates',
    'vue_path' => null,
    'has_vuegen_config' => false,
    'vuegen' => null,
];

$vuegenConfigFile = $config['current_working_directory'] . DIRECTORY_SEPARATOR . 'vuegen.json';
if (file_exists($vuegenConfigFile)) {
    $config['vuegen'] = json_decode(file_get_contents($vuegenConfigFile), true);

    $config['has_vuegen_config'] = true;

    // TODO(ssandriesser): validate if vue_path exists in config
    $vuePath = str_replace('/', DIRECTORY_SEPARATOR, $config['vuegen']['vue_path']);

    if ($vuePath[0] === DIRECTORY_SEPARATOR) {
        $config['vue_path'] = $vuePath;
    } else {
        $config['vue_path'] = $config['current_working_directory'].DIRECTORY_SEPARATOR.$vuePath;
    }
}

$config['vuegen_config_file'] = $vuegenConfigFile;

//dump($config);
$fileGenerator = new \Styriabytes\FileGenerator\FileGenerator();


$application->add(new \Vuegen\Commands\InitCommand($config, $fileGenerator));
$application->add(new \Vuegen\Commands\CreateMixinCommand($config, $fileGenerator));
$application->add(new \Vuegen\Commands\CreateComponentCommand($config, $fileGenerator));
$application->add(new \Vuegen\Commands\CreateStoreCommand($config, $fileGenerator));

$application->run();
exit;

function createDir($path)
{
    return mkdir($path, 0764, true);
}

function templates_path()
{
    return realpath(__DIR__ . '/templates');
}

$console = new \Vuegen\ConsoleOutput();
$template = new \Vuegen\Template\Engine(templates_path());

/*
|---------------------------------------------------------------------------
| Start vuegen
|---------------------------------------------------------------------------
*/

if ($argc <= 1) {
    $console->logError("wrong count of parameters!");
    $console->usage();
    exit;
}

$cmd = $argv[1];

if (!in_array($cmd, array('init', 'create'))) {
    $console->logError("{$cmd} is no valid command");
    $console->usage();
    exit;
}

// vuegen config file
$configFile = getcwd().'/vuegen.json';
$config = array(
    'vue_path' => null,
);
// Load vuegen config file if exists
if (file_exists($configFile)) {
    $config = array_merge($config, json_decode(file_get_contents($configFile), true));
}

if ($cmd === 'init') {
    if (! isset($argv[2])) {
        $console->logError("missing filepath");
        $console->usage();
        exit;
    }

    $relativeFilepath = trim($argv[2], '/');
    $filepath = getcwd().'/'.$relativeFilepath;

    if (! is_dir($filepath)) {
        $console->logError("{$filepath} is no directory");
        $console->usage();
        exit;
    }

    $vueProjectBasePath = $filepath.'/vue';
    $directories = array('model', 'extensions', 'mixins', 'shared', 'utilities',);

    if (! is_dir($vueProjectBasePath)) {
        createDir($vueProjectBasePath);
        $console->logInfo("Create directory: {$vueProjectBasePath}");
    }

    foreach ($directories as $dir) {
        $dirPath = $vueProjectBasePath.'/'.$dir;
        createDir($dirPath);
        $console->logInfo("Create directory: {$dirPath}");
    }

    // Create vuegen file
    file_put_contents($configFile, "{\n    \"vue_path\": \"{$relativeFilepath}/vue\"\n}");
    $console->logInfo("Create vuegen file");

    $console->logSuccess("init vue ready");
    exit;

} elseif ($cmd === 'create') {

    $availableFlags = array('store', 'service', 'routes');
    $selectedFlags = array();

    if (! isset($argv[2])) {
        $console->logError("missing component name");
        $console->usage();
        exit;
    }

    $args = array_slice($argv, 3);

    if (count($args) === 0 && !$config['vue_path']) {
        $console->logError("missing path to vue directory");
        $console->usage();
        exit;
    } elseif (count($args) > 1) {
        $flags = array_splice($args, 0, -1);
        foreach ($flags as $flag) {
            if (substr($flag, 0, 2) === '--' && in_array(substr($flag, 2), $availableFlags)) {
                $selectedFlags[] = substr($flag, 2);
            }
        }
    }

    if ($config['vue_path']) {
        $vueProjectBasePath = getcwd().'/'.trim($config['vue_path'], '/');
    } else {
        $vueProjectBasePath = rtrim($args[0], '/');
    }

    if (! is_dir($vueProjectBasePath)) {
        $console->logError("vue project path not found: ".$vueProjectBasePath);
        $console->usage();
        exit;
    }

    $component = array(
        'origin' => $argv[2],
        'name' => \Vuegen\Support\Str::studly($argv[2]),
    );
    $component['id'] = \Vuegen\Support\Str::kebab($component['name']);
    $component['component'] = \Vuegen\Support\Str::snake($component['name']);
    $componentPath = $vueProjectBasePath.'/'.$component['component'];

    if (is_dir($componentPath)) {
        $console->logError("component already exist");
        exit;
    }

    createDir($componentPath);
    $console->logInfo("Create directory: {$componentPath}");

    $console->logInfo('Setup default structure');
    $componentPathComponent = $componentPath.'/components';
    createDir($componentPathComponent);
    $console->logInfo("Create directory: {$componentPathComponent}");

    $file = $componentPathComponent.'/'.$component['component'].'.vue';
    $template->createFile(
        $file,
        'components/base-component',
        array_merge($component, array(
            'store' => in_array('store', $selectedFlags),
            'routes' => in_array('routes', $selectedFlags),
        ))
    );
    $console->logInfo("Create file: {$file}");

    $file = $componentPath.'/index.js';
    $template->createFile(
        $file,
        'index',
        array_merge($component, array(
            'store' => in_array('store', $selectedFlags),
            'routes' => in_array('routes', $selectedFlags),
        ))
    );
    $console->logInfo("Create file: {$file}");

    $targetFlag = 'store';
    if (in_array($targetFlag, $selectedFlags)) {
        $console->logInfo("Setup {$targetFlag} structure");
        $path = $componentPath.'/'.$targetFlag;
        createDir($path);
        $console->logInfo("Create directory: {$path}");

        $files = array(
            'index',
            'actions',
            'getters',
            'state',
            'mutations',
            'mutation_types',
        );
        foreach ($files as $f) {
            $file = $path."/{$f}.js";
            $template->createFile(
                $file,
                "store/{$f}"
            );
            $console->logInfo("Create file: {$file}");
        }
        // TODO(ssandriesser): implement store creation if structure file exists

    }

    $targetFlag = 'service';
    if (in_array($targetFlag, $selectedFlags)) {
        $console->logInfo("Setup {$targetFlag} structure");
        $path = $componentPath.'/'.$targetFlag;
        createDir($path);
        $console->logInfo("Create directory: {$path}");

        $files = array('service');
        foreach ($files as $f) {
            $name = \Vuegen\Support\Str::studly($f);
            $file = $path."/".\Vuegen\Support\Str::snake($name).".js";
            $template->createFile(
                $file,
                "service/service",
                array(
                    'name' => $name,
                )
            );
            $console->logInfo("Create file: {$file}");
        }
    }

    $targetFlag = 'routes';
    if (in_array($targetFlag, $selectedFlags)) {
        $console->logInfo("Setup {$targetFlag} structure");
        $path = $componentPath.'/views';
        createDir($path);
        $console->logInfo("Create directory: {$path}");

        $file = $path."/routes.js";
        $template->createFile(
            $file,
            "routes/routes",
            array(
                'component' => $component['component'].'_route_view',
            )
        );
        $console->logInfo("Create file: {$file}");

        $file = $componentPathComponent.'/'.$component['component'].'_route_view.vue';
        $template->createFile(
            $file,
            'components/base-component',
            array_merge($component, array(
                'store' => in_array('store', $selectedFlags),
                'routes' => false,
            ))
        );
        $console->logInfo("Create file: {$file}");
    }

    $console->logSuccess("create component {$component['origin']} ready");
    exit;
}