<?php

namespace Vuegen;

use Illuminate\Support\Str;

/**
 * Class Helper
 * @package Vuegen
 */
class Helper
{
    /**
     * @param string      $path
     * @param string|null $cwd
     *
     * @return string|null
     */
    public static function getPathExists(string $path, ?string $cwd): ?string
    {
        $cwd = $cwd ?: '';
        $targetPath = $cwd.DIRECTORY_SEPARATOR.$path;

        if (is_dir($targetPath)) {
            return $targetPath;
        }

        if (is_dir($path)) {
            return $path;
        }

        return null;
    }

    /**
     * Returns the relative path from the cwd if exists. Otherwise return the path itself
     *
     * @param string $path
     * @param string $cwd
     *
     * @return string
     */
    public static function getRelativePathIfExists(string $path, string $cwd): string
    {
        if (Str::contains($path, $cwd)) {
            return trim(Str::after($path, $cwd), DIRECTORY_SEPARATOR);
        }
        return $path;
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public static function createDir(string $path): bool
    {
        return mkdir($path, 0755, true);
    }

    /**
     * @param string $configFile
     * @param array  $config
     *
     * @return false|int
     */
    public static function updateConfigFile(string $configFile, array $config)
    {
        return file_put_contents($configFile, json_encode($config, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
    }
}