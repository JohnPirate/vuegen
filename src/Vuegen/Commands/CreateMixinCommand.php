<?php

namespace Vuegen\Commands;

use Styriabytes\FileGenerator\Blueprint\Blueprint;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateMixinCommand
 * @package Vuegen\Commands
 */
class CreateMixinCommand extends BasicCommand
{
    public const OFFSET_MIXINS = 'mixins';

    protected static $defaultName = 'create:mixin';

    protected function configure()
    {
        $this
            ->setDescription('Create a new mixin file')
            ->addArgument('mixin_name', InputArgument::REQUIRED, 'The name of the mixin.')
            ->configureVuePathArgument()
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!($vuePath = $this->getVuePath($input, $output))) {
            return 1;
        }

        $mixinName = $input->getArgument('mixin_name');
        $names = $this->getNormalizedNames($mixinName);

        $output->writeln('=== create mixin file');

        $mixinBlueprint = new Blueprint();

        $mixinTemplateFile = $this->config['templates_path'].DIRECTORY_SEPARATOR.'mixin.template.php';
        $mixinBlueprint->setTemplateFile($mixinTemplateFile);

        $mixinOutputFile = $vuePath.DIRECTORY_SEPARATOR.self::OFFSET_MIXINS.DIRECTORY_SEPARATOR.$names['file'].'.js';
        $mixinBlueprint->setOutputFile($mixinOutputFile);

        $mixinBlueprint->setData([
            'templatesPath' => $this->config['templates_path'],
            'names' => $names,
        ]);

        $this->fileGenerator->addBlueprint($mixinBlueprint);

        $this->fileGenerator->generate();

        $output->writeln('successfully create the mixin: '.$names['import']);
        return 0;
    }
}