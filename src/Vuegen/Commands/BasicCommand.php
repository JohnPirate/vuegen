<?php


namespace Vuegen\Commands;

use Illuminate\Support\Str;
use Styriabytes\FileGenerator\Blueprint\Blueprint;
use Styriabytes\FileGenerator\FileGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class BasicCommand
 * @package Vuegen\Commands
 */
class BasicCommand extends Command
{
    /** @var array */
    protected $config;

    /** @var \Styriabytes\FileGenerator\FileGenerator */
    protected $fileGenerator;

    /**
     * InitCommand constructor.
     *
     * @param array                                    $config
     * @param \Styriabytes\FileGenerator\FileGenerator $fileGenerator
     */
    public function __construct(array $config, FileGenerator $fileGenerator)
    {
        $this->config = $config;
        $this->fileGenerator = $fileGenerator;
        parent::__construct();
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return string|null
     */
    protected function getVuePath(InputInterface $input, OutputInterface $output): ?string
    {
        if (!($vuePath = $input->getArgument('vue_path')) && !($vuePath = $this->config['vue_path'])) {
            $output->writeln('missing path to vue structure');
            return null;
        }
        return $vuePath;
    }

    protected function configureVuePathArgument()
    {
        $this->addArgument('vue_path', InputArgument::OPTIONAL, 'The path to the vue files.')
        ;
    }

    /**
     * @return \Styriabytes\FileGenerator\Blueprint\Blueprint
     */
    protected function createBlueprint(): Blueprint
    {
        return new Blueprint();
    }

    /**
     * @param string $name
     *
     * @return array
     */
    public function getNormalizedNames(string $name): array
    {
        $nameStudly = Str::studly($name); // FooBar
        $nameSnake = Str::snake($nameStudly); // foo_bar
        $nameScreamingSnake = Str::upper($nameStudly); // FOO_BAR
        $nameKebab = Str::kebab($nameStudly); // foo-bar
        $nameCamel = Str::camel($nameStudly); // fooBar
        return [
            'origin' => $name,
            'file' => $nameSnake,
            'component' => $nameSnake,
            'import' => $nameStudly,
            'service' => $nameStudly,
            'id' => $nameKebab,
            'mixin' => $nameCamel,
            'const' => $nameScreamingSnake,
        ];
    }
}