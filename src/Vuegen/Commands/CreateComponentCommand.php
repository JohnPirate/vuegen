<?php

namespace Vuegen\Commands;

use Illuminate\Support\Str;
use Styriabytes\FileGenerator\Blueprint\Blueprint;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vuegen\Helper;

/**
 * Class CreateComponentCommand
 * @package Vuegen\Commands
 */
class CreateComponentCommand extends BasicCommand
{
    public const OFFSET_SHARED = 'shared';

    protected static $defaultName = 'create:component';

    protected function configure()
    {

        $this
            ->setDescription('Create a new (smart) component')
            ->addOption(
                'smart',
                null,
                InputOption::VALUE_NONE,
                'Create a smart component.'
            )
            ->addArgument(
                'component_name',
                InputArgument::REQUIRED,
                'The name of the component.'
            )
            ->configureVuePathArgument()
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!($vuePath = $this->getVuePath($input, $output))) {
            return 1;
        }

        $componentName = $input->getArgument('component_name');
        $names = $this->getNormalizedNames($componentName);

        $isSmart = $input->getOption('smart');

        $componentTemplateFile = $this->config['templates_path'].DIRECTORY_SEPARATOR.'component.template.php';
        if ($isSmart) {
            // Define structure
            $smartComponentPath = $vuePath.DIRECTORY_SEPARATOR.$names['component'];
            $smartComponentPathComponent = $smartComponentPath.DIRECTORY_SEPARATOR.'components';

            // Create structure
            if (!file_exists($smartComponentPathComponent)) {
                Helper::createDir($smartComponentPathComponent);
            }

            // Define files
            $indexTemplateFile = $this->config['templates_path'].DIRECTORY_SEPARATOR.'index.template.php';
            $indexOutputFile = $smartComponentPath.DIRECTORY_SEPARATOR.'index.js';

            $componentOutputFile = $smartComponentPathComponent.DIRECTORY_SEPARATOR.$names['component'].'.vue';
        } else {
            $componentOutputFile = $vuePath.DIRECTORY_SEPARATOR.self::OFFSET_SHARED.DIRECTORY_SEPARATOR.$names['component'].'.vue';
        }

        $sharedOptions = [
            'templatesPath' => $this->config['templates_path'],
            'names' => $names,
            'isSmart' => $isSmart,
        ];

        $output->writeln('=== create component file');

        // Component
        $componentBlueprint = new Blueprint();
        $componentBlueprint->setTemplateFile($componentTemplateFile);
        $componentBlueprint->setOutputFile($componentOutputFile);
        $componentBlueprint->setData($sharedOptions);
        $this->fileGenerator->addBlueprint($componentBlueprint);

        if ($isSmart) {
            // Create an index file for smart component
            $indexBlueprint = new Blueprint();
            $indexBlueprint->setTemplateFile($indexTemplateFile);
            $indexBlueprint->setOutputFile($indexOutputFile);
            $indexBlueprint->setData($sharedOptions);
            $this->fileGenerator->addBlueprint($indexBlueprint);
        }

        $this->fileGenerator->generate();

        $output->writeln('successfully create the component: '.$names['component']);
        return 0;
    }
}