<?php

namespace Vuegen\Commands;

use Illuminate\Support\Str;
use Styriabytes\FileGenerator\Blueprint\Blueprint;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vuegen\Helper;

/**
 * Class CreateStoreCommand
 * @package Vuegen\Commands
 */
class CreateStoreCommand extends BasicCommand
{
    public const OFFSET_STORE = 'store';
    public const OFFSET_MODULES = 'modules';

    protected static $defaultName = 'create:store';

    public static $presets = [
        'default',
        'entity',
    ];

    protected function configure()
    {

        $this
            ->setDescription('Create a new store')
            ->addOption(
                'module',
                null,
                InputOption::VALUE_NONE,
                'Create the store as a module.'
            )
            ->addOption(
                'namespaced',
                null,
                InputOption::VALUE_NONE,
                'Mark the module or the store as namespaced'
            )
            ->addOption(
                'preset',
                null,
                InputOption::VALUE_OPTIONAL,
                'Choose a preset for the module. {'.implode('|', static::$presets).'}',
                'default'
            )
            ->addArgument(
                'store_name',
                InputArgument::REQUIRED,
                'The name of the store.'
            )
            ->configureVuePathArgument()
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!($vuePath = $this->getVuePath($input, $output))) {
            return 1;
        }

        $storeName = $input->getArgument('store_name');
        $names = $this->getNormalizedNames($storeName);

        $isModule = $input->getOption('module');
        $isNamespaced = $input->getOption('namespaced');
        $preset = $input->getOption('preset');

        if (!in_array($preset, static::$presets, true)) {
            // TODO(ssandriesser): error handling
        }

        $sharedOptions = [
            'templatesPath' => $this->config['templates_path'],
            'names' => $names,
            'isNamespaced' => $isNamespaced,
            'isModule' => $isModule,
            'preset' => $preset,
        ];

        $storePath = $vuePath.DIRECTORY_SEPARATOR.self::OFFSET_STORE;

        if ($isModule) {

            // Define structure
            $storeModulePath = $storePath.DIRECTORY_SEPARATOR.self::OFFSET_MODULES;

            // Create structure
            if (!file_exists($storeModulePath)) {
                Helper::createDir($storeModulePath);
            }

            // Define files
            $storeModuleTemplateFile = $this->config['templates_path'].DIRECTORY_SEPARATOR.'store_module.template.php';
            $storModuleOutputFile = $storeModulePath.DIRECTORY_SEPARATOR.$names['file'].'.js';

            // Store Module
            $storeModuleBlueprint = new Blueprint();
            $storeModuleBlueprint->setTemplateFile($storeModuleTemplateFile);
            $storeModuleBlueprint->setOutputFile($storModuleOutputFile);
            $storeModuleBlueprint->setData($sharedOptions);
            $this->fileGenerator->addBlueprint($storeModuleBlueprint);
        } else {

            // Define files
            $storeTemplateFile = $this->config['templates_path'].DIRECTORY_SEPARATOR.'store.template.php';
            $storeOutputFile = $storePath.DIRECTORY_SEPARATOR.$names['file'].'.js';

            // Store Module
            $storBlueprint = new Blueprint();
            $storBlueprint->setTemplateFile($storeTemplateFile);
            $storBlueprint->setOutputFile($storeOutputFile);
            $storBlueprint->setData($sharedOptions);
            $this->fileGenerator->addBlueprint($storBlueprint);
        }

        $output->writeln('=== create store file');

        $this->fileGenerator->generate();

        $output->writeln('successfully create the store: '.$names['origin']);
        return 0;
    }
}