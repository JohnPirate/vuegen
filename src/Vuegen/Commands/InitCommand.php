<?php

namespace Vuegen\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vuegen\Helper;

/**
 * Class InitCommand
 * @package Vuegen\Commands
 */
class InitCommand extends BasicCommand
{
    protected static $defaultName = 'init';

    protected function configure()
    {
        $this
            ->setDescription('Init a new vue project structure')
            ->configureVuePathArgument()
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!($vuePath = $this->getVuePath($input, $output))) {
            return 1;
        }

        if (!is_array($this->config['vuegen'])) {
            $this->config['vuegen'] = [];
        }

        $this->config['vuegen']['vue_path'] = Helper::getRelativePathIfExists($vuePath, $this->config['current_working_directory']);
        if ($vuePath[0] !== DIRECTORY_SEPARATOR) {
            $vuePath = $this->config['vue_path'] = $this->config['current_working_directory'] . DIRECTORY_SEPARATOR . Helper::getRelativePathIfExists($vuePath, $this->config['current_working_directory']);
        }

        $output->writeln([
            '=== Init vue structure',
        ]);

        // Structure
        $directories = [
            'entities',
            'extensions',
            'directives',
            CreateMixinCommand::OFFSET_MIXINS,
            CreateComponentCommand::OFFSET_SHARED,
            CreateStoreCommand::OFFSET_STORE,
            'utilities',
        ];

        foreach ($directories as $directory) {

            $dir = $vuePath . DIRECTORY_SEPARATOR . $directory;

            if (!file_exists($dir)) {
                $output->writeln('- Create directory: '.$directory);
                Helper::createDir($dir);
            }
        }

        if ($this->config['has_vuegen_config']) {
            $output->writeln('- update vuegen config');
        } else {
            $output->writeln('- write vuegen config');
        }
        if (!Helper::updateConfigFile($this->config['vuegen_config_file'], $this->config['vuegen'])) {
            $output->writeln('error - not able to write config file');
            return 1;
        }
        $output->writeln('successfully initialize the vue structure');
        return 0;
    }
}