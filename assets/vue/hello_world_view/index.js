import Vue from 'vue';
import HelloWorldView from './components/hello_world_view';

const elementId = 'hello-world-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            HelloWorldView,
        },

        data() {
            // const envData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
                // foo: envData.foo,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    // foo: this.foo,
                }
            });
        }
    });
});