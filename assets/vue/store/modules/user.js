/*
|---------------------------------------------------------------------------
| Vuex Store Module - User
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {
    SET_USER: 'SET_USER',
    UPDATE_USER: 'UPDATE_USER',
    DELETE_USER: 'DELETE_USER',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_USER](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_USER](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_USER](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};


// Export module
export const UserModule = {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};