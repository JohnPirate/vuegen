/*
|---------------------------------------------------------------------------
| Vuex Store Module - Event
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {
    SET_EVENT: 'SET_EVENT',
    UPDATE_EVENT: 'UPDATE_EVENT',
    DELETE_EVENT: 'DELETE_EVENT',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_EVENT](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_EVENT](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_EVENT](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};

export const EventModule = {
    namespaced: true,
    state,
    mutations,
    getters,
};