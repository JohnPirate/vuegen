/*
|---------------------------------------------------------------------------
| Vuex Store Module - Task
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {
    SET_TASK: 'SET_TASK',
    UPDATE_TASK: 'UPDATE_TASK',
    DELETE_TASK: 'DELETE_TASK',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_TASK](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_TASK](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_TASK](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};

export const TaskModule = {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};