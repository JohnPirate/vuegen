/*
|---------------------------------------------------------------------------
| Vuex Store Module - Project
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {
    SET_PROJECT: 'SET_PROJECT',
    UPDATE_PROJECT: 'UPDATE_PROJECT',
    DELETE_PROJECT: 'DELETE_PROJECT',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_PROJECT](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_PROJECT](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_PROJECT](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};

export const ProjectModule = {
    namespaced: true,
    state,
    mutations,
    getters,
};