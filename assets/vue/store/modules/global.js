/*
|---------------------------------------------------------------------------
| Vuex Store Module - Global
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {};


// Module state
const state = () => {
    return {};
};


// Module getters
const getters = {};


// Module mutations
const mutations = {};


// Module actions
const actions = {};

export const GlobalModule = {
    state,
    mutations,
    getters,
    actions,
};