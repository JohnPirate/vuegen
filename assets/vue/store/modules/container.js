/*
|---------------------------------------------------------------------------
| Vuex Store Module - Container
|---------------------------------------------------------------------------
*/

import Vue from 'vue';

// Module mutation types
export const types = {
    SET_CONTAINER: 'SET_CONTAINER',
    UPDATE_CONTAINER: 'UPDATE_CONTAINER',
    DELETE_CONTAINER: 'DELETE_CONTAINER',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_CONTAINER](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_CONTAINER](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_CONTAINER](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};

export const ContainerModule = {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};