import Vue from 'vue';
import Vuex from 'vuex';
import {ContainerModule} from './modules/container';
import {ProjectModule} from './modules/project';
import {TaskModule} from './modules/task';
import {EventModule} from './modules/event';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export const createStore = () => new Vuex.Store({
    module: {
        container: ContainerModule,
        project: ProjectModule,
        task: TaskModule,
        event: EventModule,
    },
    strict: debug,
});
export default createStore();