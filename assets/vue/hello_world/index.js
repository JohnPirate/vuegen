import Vue from 'vue';
import HelloWorld from './components/hello_world';

const elementId = 'hello-world';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            HelloWorld,
        },

        data() {
            // const envData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
                // foo: envData.foo,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    // foo: this.foo,
                }
            });
        }
    });
});