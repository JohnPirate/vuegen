<?php
use Styriabytes\FileGenerator\Support\TemplateTools;
?>
/*
|---------------------------------------------------------------------------
| Vuex Store Module - <?= $names['import'].PHP_EOL ?>
|---------------------------------------------------------------------------
*/

<?php TemplateTools::includeTemplate($templatesPath.DIRECTORY_SEPARATOR.'shared/store_module_'.$preset.'_preset.template.php', $data_); ?>


// Export module
export const <?= $names['import'] ?>Module = {
<?php if ($isNamespaced): ?>
    namespaced: true,
<?php endif; ?>
    state,
    mutations,
    getters,
    actions,
};