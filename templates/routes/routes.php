import VueRouter from 'vue-router';
Vue.use(VueRouter); // Install plugin

/*
 |--------------------------------------------------------------------------
 | Main routes
 |--------------------------------------------------------------------------
 */
let mainRoutes = [
    {
        path: '/',
        component: require(`../components/<?= $component ?>`),
        props: (route) => {
            return {
                anyComponentProperty: 'hello world',
            }
        }
    },
    // {
    //     path: '/:anyParam',
    //     component: require(`../components/<?= $component ?>`),
    //     props: (route) => {
    //         return {
    //             anyComponentProperty: 'hello world',
    //             anotherComponentProperty: route.params.anyParam,
    //         }
    //     }
    // },
    {
        path: '*',
        redirect: '/'
    }
];

/*
 |--------------------------------------------------------------------------
 | Sub routes
 |--------------------------------------------------------------------------
 */
// let subRoutes = [];

export default new VueRouter({
    // mode: 'history', // Can be used if you don't like hashtaged urls
    base: '/',   // baseroute
    routes: mainRoutes,
    linkActiveClass: 'active'
});