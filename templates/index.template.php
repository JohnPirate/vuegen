import Vue from 'vue';
import <?= $names['import'] ?> from './components/<?= $names['component'] ?>';

const elementId = '<?= $names['id'] ?>';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            <?= $names['import'] ?>,
        },

        data() {
            // const envData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
                // foo: envData.foo,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    // foo: this.foo,
                }
            });
        }
    });
});