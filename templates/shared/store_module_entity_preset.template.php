import Vue from 'vue';

// Module mutation types
export const types = {
    SET_<?= $names['const'] ?>: 'SET_<?= $names['const'] ?>',
    UPDATE_<?= $names['const'] ?>: 'UPDATE_<?= $names['const'] ?>',
    DELETE_<?= $names['const'] ?>: 'DELETE_<?= $names['const'] ?>',
};


// Module state
const state = () => {
    return {
        data: {},
    };
};


// Module getters
const getters = {
    data: (state) => state.data,
};


// Module mutations
const mutations = {
    [types.SET_<?= $names['const'] ?>](state, payload) {
        Vue.set(state.data, payload.id, payload);
    },
    [types.UPDATE_<?= $names['const'] ?>](state, payload) {
        Vue.set(state.data[payload.id], payload.key, payload.value);
    },
    [types.DELETE_<?= $names['const'] ?>](state, payload) {
        Vue.delete(state.data, payload);
    },
};


// Module actions
const actions = {

};
