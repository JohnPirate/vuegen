<?php
use Styriabytes\FileGenerator\Support\TemplateTools;
?>
<template>

</template>

<script>
<?php if($isSmart): ?>

    /*
    |---------------------------------------------------------------------------
    | Smart component
    |---------------------------------------------------------------------------
    */
<?php endif; ?>

    export default {

        name: '<?= $names['component'] ?>',

<?= TemplateTools::prefixLines(TemplateTools::getTemplateContent($templatesPath.DIRECTORY_SEPARATOR.'shared/vue_component_setup.template.php', $data_), '    ') ?>

    }

</script>

<style lang="scss">

</style>
