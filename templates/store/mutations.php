import * as types from './mutation_types';

export default {
    [types.SOMETHING](state, something) {
        state.something = something;
    },
};