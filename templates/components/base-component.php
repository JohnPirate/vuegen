<template>
<?php if ($routes): ?>
    <transition name="fade" mode="out-in">
        <router-view></router-view>
    </transition>
<?php endif; ?>
</template>

<script>

    /*
    |---------------------------------------------------------------------------
    | <?php if ($routes): ?>Router<?php else: ?>Smart<?php endif; ?> component
    |---------------------------------------------------------------------------
    */

    <?php if ($store && !$routes): ?>import { mapActions, mapGetters, mapState } from 'vuex';<?php echo PHP_EOL; endif; ?>

    export default {

        name: "<?= $component ?>",

        /*
         |---------------------------------------------------------------------------
         | Composition
         |---------------------------------------------------------------------------
         */

        // mixins: [],

        // extends: {},

        /*
         |---------------------------------------------------------------------------
         | Assets
         |---------------------------------------------------------------------------
         */

        // directives: {},

        // filters: {},

        // components: {},

        /*
         |---------------------------------------------------------------------------
         | Lifecycle Hooks
         |---------------------------------------------------------------------------
         */

        // created() {},

        // mounted() {},

        /*
         |---------------------------------------------------------------------------
         | Data
         |---------------------------------------------------------------------------
         */

        // props: {},

        // data() { return {}; },<?php if ($store && !$routes): ?><?= PHP_EOL ?>

        computed: {
            // ...mapState([
            //     '',
            // ]),

            ...mapGetters([
                // '',
            ]),
        },

        // watch: {},

        methods: {
            ...mapActions([
                // '',
            ]),
        },<?php elseif (!$store): ?><?= PHP_EOL ?>

        // computed: {},

        // watch: {},

        // methods: {},
        <?php endif; ?><?= PHP_EOL ?>
    }

</script>
<?php if (!$routes): ?>

<style lang="scss">

</style>
<?php endif; ?>
