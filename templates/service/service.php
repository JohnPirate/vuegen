import axios from 'axios';

/**
 * <?= $name ?>Service
 * @type {Object}
 */
export default {

    /**
     * @returns {AxiosPromise<any>}
     */
    fetch(data) {
        let url = `/`;
        return axios.get(url, data);
    }
}