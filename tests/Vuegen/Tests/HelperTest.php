<?php declare(strict_types=1);

namespace Vuegen\Tests;

use PHPUnit\Framework\TestCase;
use Vuegen\Helper;

final class HelperTest extends TestCase
{
    public function testGetRelativePath()
    {
        $cwd = getcwd();

        $expectedRelativePath = Helper::getRelativePathIfExists('asset/vue', $cwd);
        $this->assertEquals('asset/vue', $expectedRelativePath);

        $expectedRelativePath = Helper::getRelativePathIfExists($cwd.'/asset/vue', $cwd);
        $this->assertEquals('asset/vue', $expectedRelativePath);

        $expectedRelativePath = Helper::getRelativePathIfExists('/var/www/test/asset/vue', $cwd);
        $this->assertEquals('/var/www/test/asset/vue', $expectedRelativePath);
    }
}